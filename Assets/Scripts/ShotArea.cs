using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotArea : MonoBehaviour
{
    private float forceApplied = 50;

	private void OnTriggerStay2D(Collider2D col)
	{
        Debug.Log("COL2111");
        if (col.gameObject.tag == "Ball")
        {
            Debug.Log("COL11");
            if (Input.GetKey(KeyCode.LeftShift))
            {
                Debug.Log("SHOT111");
                col.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(forceApplied,0f));
            }
        }
    }
}
