using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	private float speed = 500f;

	void Start()
	{
		SignalRClient.Instance.JoinRoom();
	}

	void Update()
	{
		float xDirection = Input.GetAxisRaw("Horizontal");
		float yDirection = Input.GetAxisRaw("Vertical");

		Vector2 moveDirection = new Vector2(xDirection, yDirection);
		

		moveDirection.Normalize();

		if (moveDirection == Vector2.zero)
		{
			if (SignalRClient.Instance.IsTransformChanged())
			{
				Move(SignalRClient.Instance.GetCurrentTransform());
			}
		}
		else
		{
			//var newPosition = new Vector2(transform.position.x + moveDirection.x, transform.position.y + moveDirection.y);

			//transform.Translate(moveDirection * speed * Time.deltaTime);

			//transform.position = Vector2.MoveTowards(transform.position, newPosition, Time.deltaTime * speed);

			GetComponent<Rigidbody2D>().AddForce(moveDirection * speed * Time.deltaTime,ForceMode2D.Force);
			SignalRClient.Instance.SendTransform(transform.position.x, transform.position.y);
		}
	}

	public void Move(Vector2 newPosition)
	{
		transform.position = newPosition;
	}
}
