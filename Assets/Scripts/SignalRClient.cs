using Microsoft.AspNetCore.SignalR.Client;
using System.Collections.Generic;
using UnityEngine;

public class SignalRClient : MonoBehaviour
{
	public static SignalRClient Instance;
	private HubConnection _connection;

	private Vector2 currentTransform;
	private bool isTransformChanged;

	private List<string> _rooms = new List<string>();

	void Awake()
	{
		Debug.Log("SignalRClient awake");
		//string url = @"http://localhost:8080/";

		string url = @"https://localhost:7135/gamehub";

		_connection = new HubConnectionBuilder()
			.WithUrl(url)
			.Build();

		_connection.StartAsync();

		_connection.On<string>("AddMessage", (msg) => ShowMessage(msg));

		_connection.On<float, float>("GetTransformation", (x, y) => GetTransformation(x, y));

		_connection.On<List<string>>("GetRooms", (List<string> rooms) => GetRooms(rooms));

		Instance = this;
	}

	void Start()
	{
		DontDestroyOnLoad(this);
		isTransformChanged = false;
	}

	void OnDestroy()
	{
		_connection.DisposeAsync();
	}

	public void JoinRoom()
	{
		_connection.SendAsync("JoinRoom","TEST");
	}

	public void SendTransform(float x, float z)
	{
		//Debug.Log("x " + x + " , z " + z);
		_connection.SendAsync("SendTransformation", x, z);
	}
	public void GetTransformation(float x, float y)
	{
		//Debug.Log("Received opponent position: X = " + x + ", Y = " + y);

		Vector2 newTransform = new Vector2(x, y);

		if (newTransform != currentTransform)
		{
			isTransformChanged = true;
			currentTransform = newTransform;
		}
	}

	public void SendRoomListRequest()
	{
		_connection.SendAsync("SendRooms");
	}

	public void GetRooms(List<string> rooms)
	{
		_rooms = rooms;
	}

	public void ShowRooms()
	{
		Debug.Log("CLICKED");
		SendRoomListRequest();
		foreach (var item in _rooms)
		{
			Debug.Log(item);
		}
	}

	public void ShowMessage(string msg)
	{
		Debug.Log(msg);
	}

	public bool IsTransformChanged()
	{
		return isTransformChanged;
	}

	public Vector2 GetCurrentTransform()
	{
		isTransformChanged = false;
		return currentTransform;
	}
}
